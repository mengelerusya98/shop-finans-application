import firebase from 'firebase/compat/app'
import 'firebase/compat/auth'
import 'firebase/compat/firestore'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyABLvIh9KexqgaYT8pS3mtnqz3reygm3yI',
  authDomain: 'my-shop-application-59541.firebaseapp.com',
  projectId: 'my-shop-application-59541',
  storageBucket: 'my-shop-application-59541.appspot.com',
  messagingSenderId: '954597309245',
  appId: '1:954597309245:web:ce8443f0bc222de628a354',
  measurementId: 'G-V1P3SPLMFH'
}
firebase.initializeApp(firebaseConfig)

firebase.getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = firebase.auth().onAuthStateChanged(user => {
      unsubscribe()
      resolve(user)
    }, reject)
  })
}

export const db = firebase.firestore()
db.settings({
  timestampsInSnapshots: true
})
