import { defineStore } from 'pinia'
import { db } from 'boot/firebase'
import exportFromJSON from 'export-from-json'

const _dbp = db.collection('purchases')
const _dbf = db.collection('profits')
export const mainStore = defineStore('counter', {
  state: () => ({
    purchases: null,
    profits: null
  }),
  getters: { },
  actions: {
    async isReportToday () {
      let isReport = null
      await _dbf.get().then(report => {
        report.docs.forEach(rep => {
          if (new Date(rep.data().date.seconds * 1000).toLocaleDateString() ===
            new Date().toLocaleDateString()) {
            isReport = rep.data()
          }
        })
      })
      return isReport
    },
    // PURCHASE
    getNowPurchases () {
      this.purchases = []
      _dbp.get().then(res => {
        res.docs.forEach(doc => {
          if (new Date(doc.data().createAt.seconds * 1000).toLocaleDateString() ===
            new Date().toLocaleDateString()) {
            const arr = doc.data()
            arr.id = doc.id
            this.purchases.push(arr)
          }
        })
      }).catch(err => {
        console.log('GET PURCHASES ERROR: ', err)
      })
    },
    getPurchases (filter, type) {
      this.purchases = []
      switch (type) {
        case 'date':
          _dbp.get().then(res => {
            res.docs
              .filter(el => new Date(el.data().createAt.seconds * 1000).toLocaleDateString() === filter)
              .forEach((doc, index) => {
                const arr = doc.data()
                arr.id = doc.id
                arr.index = index + 1
                this.purchases.push(arr)
              })
          })
          break
        case 'month':
          _dbp.get().then(res => {
            res.docs
              .filter(el => new Date(el.data().createAt.seconds * 1000).getMonth() === filter.value)
              .forEach((doc, index) => {
                const arr = doc.data()
                arr.id = doc.id
                arr.index = index + 1
                this.purchases.push(arr)
              })
          })
          break
        default:
          _dbp.get().then(res => {
            res.docs.forEach((doc, index) => {
              const arr = doc.data()
              arr.id = doc.id
              arr.index = index + 1
              this.purchases.push(arr)
            })
          }).catch(err => {
            console.log('GET PURCHASES ERROR: ', err)
          })
      }
    },
    addPurchaseItem (item) {
      _dbp.add(item).then(() => {
        console.log('SUCCESS')
      }).catch(err => {
        console.log('ADD PURCHASE ITEM: ', err)
      })
    },
    deletePurchaseItem (itemId) {
      _dbp.doc(itemId).delete().then(() => {})
    },
    // PROFIT
    getProfits (filter, type) {
      this.profits = []
      switch (type) {
        case 'date':
          _dbf.get().then(res => {
            res.docs
              .filter(el => new Date(el.data().date.seconds * 1000).toLocaleDateString() === filter)
              .forEach((doc, index) => {
                const arr = doc.data()
                arr.id = doc.id
                arr.index = index + 1
                this.profits.push(arr)
              })
          })
          break
        case 'month':
          _dbf.get().then(res => {
            res.docs
              .filter(el => new Date(el.data().date.seconds * 1000).getMonth() === filter.value)
              .forEach((doc, index) => {
                const arr = doc.data()
                arr.id = doc.id
                arr.index = index + 1
                this.purchases.push(arr)
              })
          })
          break
        default:
          _dbf.get().then(res => {
            res.docs.forEach((el, index) => {
              const obj = el.data()
              obj.id = el.id
              obj.index = index + 1
              this.profits.push(obj)
            })
          }).catch(err => {
            console.log('GET PURCHASES ERROR: ', err)
          })
      }
    },
    addProfitItem (item) {
      _dbf.add(item).then(() => {})
    }
  }
})

export const excelParser = () => {
  function exportDataFromJSON (data, newFileName, fileExportType) {
    if (!data) return
    try {
      const fileName = newFileName || 'exported-data'
      const exportType = exportFromJSON.types[fileExportType || 'xls']
      exportFromJSON({
        data,
        fileName,
        exportType
      })
    } catch (e) { throw new Error('Parsing failed!') }
  }
  return { exportDataFromJSON }
}
